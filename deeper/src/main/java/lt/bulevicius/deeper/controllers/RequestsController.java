package lt.bulevicius.deeper.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/admin")
public class RequestsController {
	
	private int countRequests;

	@RequestMapping(value = "/requestsQuantity", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "admin user", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 401, message = "You are not authorized access the resource"),
			@ApiResponse(code = 404, message = "The resource not found") })
	public String getRequestsQuantity() {
		incrementRequests();
		String requests = null;
		requests = "Now is beeing processed " + countRequests + " request(s)";
		decrementRequests();
		return requests;
	}

	public void incrementRequests(){
		countRequests++;
	}
	
	public void decrementRequests(){
		countRequests--;
	}

}
