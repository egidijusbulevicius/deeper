package lt.bulevicius.deeper.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lt.bulevicius.deeper.line.Line;
import lt.bulevicius.deeper.point.Point;
import lt.bulevicius.deeper.service.impl.IntersectionService;
import lt.bulevicius.deeper.square.Square;

@RestController
@RequestMapping("/guest")
public class IntersectionController {

	@Autowired
	IntersectionService intersectionService;

	StringBuilder intPoints = new StringBuilder();

	// the first 8 parameters are 4 dots of square within x and y values, the
	// last 4 parameters are lines dots within x and y values
	@RequestMapping("/intersectionPoints/{ax}/{ay}/{bx}/{by}/{cx}/{cy}/{dx}/{dy}/{ex}/{ey}/{fx}/{fy}/")
	public String getIntersectionPoint(@PathVariable double ax, @PathVariable double ay, @PathVariable double bx,
			@PathVariable double by, @PathVariable double cx, @PathVariable double cy, @PathVariable double dx,
			@PathVariable double dy, @PathVariable double ex, @PathVariable double ey, @PathVariable double fx,
			@PathVariable double fy) {
		Square square = new Square();
		square.setAx(ax);
		square.setAy(ay);
		square.setBx(bx);
		square.setBy(by);
		square.setCx(cx);
		square.setCy(cy);
		square.setDx(dx);
		square.setDy(dy);
		Line line = new Line();
		line.setEx(ex);
		line.setEy(ey);
		line.setFx(fx);
		line.setFy(fy);
		intPoints.setLength(0);
		List<Point> intersectionDots = intersectionService.getIntersectionPoints(square, line);
		if (intersectionDots.isEmpty()){
			return "The first figure is not square or do not intersect";
		}
		for (Point intersectionPoint : intersectionDots) {
			intPoints.append("x = " + intersectionPoint.getX() + " y = " + intersectionPoint.getY());
		}
		return intPoints.toString();
	}
}
