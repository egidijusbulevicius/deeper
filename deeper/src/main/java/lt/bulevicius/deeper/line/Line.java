package lt.bulevicius.deeper.line;

public class Line {
	
	private double ex;
	private double ey;
	private double fx;
	private double fy;
	
	public double getEx() {
		return ex;
	}
	public void setEx(double ex) {
		this.ex = ex;
	}
	public double getEy() {
		return ey;
	}
	public void setEy(double ey) {
		this.ey = ey;
	}
	public double getFx() {
		return fx;
	}
	public void setFx(double fx) {
		this.fx = fx;
	}
	public double getFy() {
		return fy;
	}
	public void setFy(double fy) {
		this.fy = fy;
	}

}
