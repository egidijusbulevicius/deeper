package lt.bulevicius.deeper.service.impl;

import java.util.List;

import lt.bulevicius.deeper.line.Line;
import lt.bulevicius.deeper.point.Point;
import lt.bulevicius.deeper.square.Square;

public interface IntersectionService {

	public List<Point> getIntersectionPoints(Square square, Line line);

}
