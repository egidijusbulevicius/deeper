package lt.bulevicius.deeper.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import lt.bulevicius.deeper.line.Line;
import lt.bulevicius.deeper.point.Point;
import lt.bulevicius.deeper.square.Square;

@Service
public class IntersectionServiceImpl implements IntersectionService {

	private List<Point> points = new ArrayList<>();
	private List<Point> intersectionPoints = new ArrayList<>();

	@Override
	public List<Point> getIntersectionPoints(Square square, Line line) {

		if (isSquareLinesEquability(square)) {
			if (isFigureSquare(square)) {
				// pass each square line with intersected line
				points.add(getIntersectionPoint(square.getAx(), square.getAy(), square.getBx(), square.getBy(),
						line.getEx(), line.getEy(), line.getFx(), line.getFy()));
				points.add(getIntersectionPoint(square.getBx(), square.getBy(), square.getDx(), square.getDy(),
						line.getEx(), line.getEy(), line.getFx(), line.getFy()));
				points.add(getIntersectionPoint(square.getDx(), square.getDy(), square.getCx(), square.getCy(),
						line.getEx(), line.getEy(), line.getFx(), line.getFy()));
				points.add(getIntersectionPoint(square.getAx(), square.getAy(), square.getCx(), square.getCy(),
						line.getEx(), line.getEy(), line.getFx(), line.getFy()));
			}
		}
		for (Point p : points) {
			if (p != null) {
				intersectionPoints.add(p);
			}
		}

		return intersectionPoints;
	}

	private boolean isSquareLinesEquability(Square square) {
		// check if lines are equal
		if (square.getBx() - square.getAx() == square.getBy() - square.getDy()
				&& square.getDx() - square.getCx() == square.getAy() - square.getCy()) {
			return true;
		}
		return false;
	}

	private boolean isFigureSquare(Square square) {
		// if diagonals are equal then figure is square 
		if (Math.sqrt(Math.pow(square.getBx() - square.getAx(), 2)
				+ Math.pow(square.getBy() - square.getDy(), 2)) == Math.sqrt(
						Math.pow(square.getBx() - square.getAx(), 2) + Math.pow(square.getAy() - square.getCy(), 2))) {
			return true;
		}
		return false;
	}

	public Point getIntersectionPoint(double ax, double ay, double bx, double by, double cx, double cy, double dx,
			double dy) {

		Point p = new Point();
		if ((by - ay) * (dx - cx) == (dy - cy) * (bx - ax)) {
			// parallel
			return null;
		} else {
			double m1 = (by - ay) / (bx - ax);
			double m2 = (dy - cy) / (dx - cx);
			// Find point of intersection
			double x, y;
			if (Double.isInfinite(m1)) {
				x = ax;
				y = m2 * (x - cx) + cy;
			} else if (Double.isInfinite(m2)) {
				x = cx;
				y = m1 * (x - ax) + ay;
			} else {
				x = (m1 * ax - ay - m2 * cx + cy) / (m1 - m2);
				y = m1 * (x - ax) + ay;
			}
			if (((x >= ax && x <= bx) || (x <= ax && x >= bx)) && ((y >= ay && y <= by) || (y <= ay && y >= by))
					&& ((x >= cx && x <= dx) || (x <= cx && x >= dx))
					&& ((y >= cy && y <= dy) || (y <= cy && y >= dy))) {
				// intersect
				p.setX(x);
				p.setY(y);
			} else {
				// do not intersect
				p = null;
			}
		}
		return p;
	}

}
